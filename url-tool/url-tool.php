<?php
class UrlTool
{
	private $originalUrl;

	public function getOriginalUrl(): string
	{
		return $this->originalUrl;
	}

	public function setOriginalUrl(string $url): void
	{
		$this->originalUrl = $url;
	}

	public function encodeUrl(): string
	{
		return urlencode($this->originalUrl);
	}

	public function decodeUrl(): string
	{
		return urldecode($this->originalUrl);
	}
}
