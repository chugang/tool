<?php
/**
 * 中缀表达式，转为后缀表达式
 * 耗费时间 2小时40分
 */
class Expression
{
	public function clearStr($str)
	{
		$length = strlen($str);
		$newStr = '';
		for($i = 0; $i < $length; $i++){
			$oneStr = $str[$i];
			if($oneStr === " "){
				continue;
			}
			$newStr .= $oneStr;
		}

		$newStr = $this->checkNumber($newStr);	
		return $newStr;
	}

	public function checkNumber($str)
	{
		$length = strlen($str);
		$newStr = [];
		for($i = 0; $i < $length; $i++){	
			
			$oneStr = $str[$i];
			if(is_numeric($oneStr)){						
				$numberStr = '';
				for($j = $i; $j < $length; $j++){
					if(is_numeric($str[$j])){
						$numberStr .= $str[$j];
					}else{	
						$i = $j-1;
						break;
					}

					
				}

				if(!empty($numberStr)){
					$newStr[] = $numberStr;
				}						
				
			}else{				
				$newStr[] = $oneStr;
			}			
		}

		return $newStr;
	}

	public function infixExpression2SuffixExpression($expression)
	{
		$expression = $this->clearStr($expression);

		$length = count($expression);
		$stack = [];
		$result = [];

		for($i = 0; $i < $length; $i++){
			$oneStr = $expression[$i];
			
			if(is_numeric($oneStr)){

				$result[] = $oneStr;

			}elseif(in_array($oneStr, ['+', '-', '*', '/'])){

				$tmp = $stack;
				$top = array_pop($tmp);				
				if($top && in_array($oneStr, ['+', '-']) && in_array($top, ['*', '/'])){
					while($tmpTop = array_pop($stack)){
						$result[] = $tmpTop;
					}					
				}

				$stack[] = $oneStr;

			}elseif($oneStr == '('){

				$stack[] = $oneStr;				

			}elseif($oneStr == ')'){

				$count = count($stack);
				$key = array_search('(', $stack);
				$tmpKey = $key;
				while($tmpKey < $count){
					$tmpKey++;
					$tmpTop = array_pop($stack);					
					if($tmpTop == '('){
						continue;
					}
					$result[] = $tmpTop;					
				}				
							
			}else{
				$result[] = $oneStr;
			}
		}

		while($tmpTop = array_pop($stack)){
			$result[] = $tmpTop;
		}	

		return $result;
	}
}

// test
/*
$expression = new Expression();
$str = "9 + (3-1) * 3 + 10 / 2 + 5 + 7 - 8";
$newStr = $expression->infixExpression2SuffixExpression($str);
var_dump($newStr);exit;
echo $newStr;
*/