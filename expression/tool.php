<html>
<head>
<meta charset="utf-8">
<style>
#content
{
	width:400px;
	height:300px;
}
#result
{
	margin-top: 50px;
}
</style>
</head>
<body>
<?php
if(isset($_POST['content']) && !empty($_POST['content'])){
	$content = $_POST['content'];
}else{
	$content = '';
}
?>
<div id="content">
	<form method="post">
		<p>			
			<input type="text" value="<?php echo $content; ?>" name="content" />
		</p>
		<p><input type="submit" value="提交" /></p>
	</form>
</div>
<div id="result">
<?php require_once 'expression.php'; ?>
<?php if(!empty($content)): ?>
	<?php $expression = new Expression(); ?>
	<?php $result = $expression->infixExpression2SuffixExpression($content); ?>
	<h1><?php echo implode($result, ' '); ?></h1>
<?php endif; ?>
</div>
</body>
</html>